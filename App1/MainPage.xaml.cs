﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.Devices.Gpio;
using System;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Core;
using System.Numerics;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>

    public sealed partial class MainPage : Page
    {
        private SolidColorBrush redBrush = new SolidColorBrush(Windows.UI.Colors.Red);
        private SolidColorBrush grayBrush = new SolidColorBrush(Windows.UI.Colors.LightGray);

        // #1, Pin 40, GPIO #21
        private const int contact_pin1 = 21;
        private GpioPin pin;
        private GpioPinValue pinValue;

        // #2, Pin 37, GPIO #26
        private const int contact_pin2 = 26;
        private GpioPin pin2;
        private GpioPinValue pinValue2;

        // #3, Pin 35, GPIO #19
        private const int contact_pin3 = 19;
        private GpioPin pin3;
        private GpioPinValue pinValue3;

        // #4, Pin 33, GPIO #13
        private const int contact_pin4 = 13;
        private GpioPin pin4;
        private GpioPinValue pinValue4;

        // #5, Pin 22, GPIO #25
        private const int contact_pin5 = 25;
        private GpioPin pin5;
        private GpioPinValue pinValue5;

        // #6, Pin 32, GPIO #12
        private const int contact_pin6 = 12;
        private GpioPin pin6;
        private GpioPinValue pinValue6;

      /*  struct GPIO
        {
            //Contactor connects
            int contact_pin;
            GpioPin contact_gpio;
            GpioPinValue contact_val;
            //Timer ADJ
            int tadj_gpio;
            GpioPin tadj_pin;
            GpioPinValue tadj_val;

        };

        Vector<GPIO> GPIO_Vector;*/

        // #TADJ, Pins 31, 29, 15, 13 , GPIO #6, 5, 22, 27
        private const int TADJ_PIN1 = 6;
        private const int TADJ_PIN2 = 5;
        private const int TADJ_PIN3 = 22;
        private const int TADJ_PIN4 = 27;
        private GpioPin pinTADJ1;
        private GpioPin pinTADJ2;
        private GpioPin pinTADJ3;
        private GpioPin pinTADJ4;
        private GpioPinValue pinValueTADJ1;
        private GpioPinValue pinValueTADJ2;
        private GpioPinValue pinValueTADJ3;
        private GpioPinValue pinValueTADJ4;

        private int TADJ_POS = 0;

           //LED_TO_PI's
           //1
           private const int LED2PI_PIN1 = 24;
           private GpioPin led2pi_Pin1;
           private GpioPinValue led2piPinValue1 = GpioPinValue.High;
           //2
           private const int LED2PI_PIN2 = 23;
           private GpioPin led2pi_Pin2;
           private GpioPinValue led2piPinValue2 = GpioPinValue.High;
           //3
           private const int LED2PI_PIN3 = 18;
           private GpioPin led2pi_Pin3;
           private GpioPinValue led2piPinValue3 = GpioPinValue.High;
           //4
           private const int LED2PI_PIN4 = 4;
           private GpioPin led2pi_Pin4;
           private GpioPinValue led2piPinValue4 = GpioPinValue.High;
                
        public MainPage()
        {
            InitGPIO();
            InitializeComponent();//==============got rid of this.============================

          //  GPIO_Vector[0]
        }

        ~MainPage()
        {
            pinValueTADJ4 = GpioPinValue.Low;
            pinValueTADJ3 = GpioPinValue.Low;
            pinValueTADJ2 = GpioPinValue.Low;
            pinValueTADJ1 = GpioPinValue.Low;
            pinTADJ4.Write(pinValueTADJ4);
            pinTADJ3.Write(pinValueTADJ3);
            pinTADJ2.Write(pinValueTADJ2);
            pinTADJ1.Write(pinValueTADJ1);
            TADJ_POS = 0;
        }
        private void InitGPIO()
        {
            var gpio = GpioController.GetDefault();

            // Show an error if there is no GPIO controller
            if (gpio == null)
            {
                pin = null;
                // GpioStatus.Text = "There is no GPIO controller on this device.";=======Uselesss, delete later==========
                return;
            }

            //LED_TO_PI_Listeners---------------------------------------------------------------------
            led2pi_Pin1 = gpio.OpenPin(LED2PI_PIN1);
            led2pi_Pin2 = gpio.OpenPin(LED2PI_PIN2);
            led2pi_Pin3 = gpio.OpenPin(LED2PI_PIN3);
            led2pi_Pin4 = gpio.OpenPin(LED2PI_PIN4);

            // Check if input pull-up resistors are supported
            if (led2pi_Pin1.IsDriveModeSupported(GpioPinDriveMode.InputPullUp))
                led2pi_Pin1.SetDriveMode(GpioPinDriveMode.InputPullUp);
            else
                led2pi_Pin1.SetDriveMode(GpioPinDriveMode.Input);

            if (led2pi_Pin2.IsDriveModeSupported(GpioPinDriveMode.InputPullUp))
                led2pi_Pin2.SetDriveMode(GpioPinDriveMode.InputPullUp);
            else
                led2pi_Pin2.SetDriveMode(GpioPinDriveMode.Input);

            if (led2pi_Pin3.IsDriveModeSupported(GpioPinDriveMode.InputPullUp))
                led2pi_Pin3.SetDriveMode(GpioPinDriveMode.InputPullUp);
            else
                led2pi_Pin3.SetDriveMode(GpioPinDriveMode.Input);

            if (led2pi_Pin4.IsDriveModeSupported(GpioPinDriveMode.InputPullUp))
                led2pi_Pin4.SetDriveMode(GpioPinDriveMode.InputPullUp);
            else
                led2pi_Pin4.SetDriveMode(GpioPinDriveMode.Input);

            // Set a debounce timeout to filter out switch bounce noise from a button press
            led2pi_Pin1.DebounceTimeout = TimeSpan.FromMilliseconds(50);
            led2pi_Pin2.DebounceTimeout = TimeSpan.FromMilliseconds(50);
            led2pi_Pin3.DebounceTimeout = TimeSpan.FromMilliseconds(50);
            led2pi_Pin4.DebounceTimeout = TimeSpan.FromMilliseconds(50);

            // Register for the ValueChanged event so our buttonPin_ValueChanged 
            // function is called when the button is pressed
            led2pi_Pin1.ValueChanged += led2pi_Pin1_ValueChanged;
            led2pi_Pin2.ValueChanged += led2pi_Pin2_ValueChanged;
            led2pi_Pin3.ValueChanged += led2pi_Pin3_ValueChanged;
            led2pi_Pin4.ValueChanged += led2pi_Pin4_ValueChanged;
            //LED_TO_PI_Listeners---------------------------------------------------------------------

            //#1
            pin = gpio.OpenPin(contact_pin1);
            pinValue = GpioPinValue.Low;
            pin.Write(pinValue);
            pin.SetDriveMode(GpioPinDriveMode.Output);

            //#2
            pin2 = gpio.OpenPin(contact_pin2);
            pinValue2 = GpioPinValue.Low;
            pin2.Write(pinValue2);
            pin2.SetDriveMode(GpioPinDriveMode.Output);

            //#3
            pin3 = gpio.OpenPin(contact_pin3);
            pinValue3 = GpioPinValue.Low;
            pin3.Write(pinValue3);
            pin3.SetDriveMode(GpioPinDriveMode.Output);

            //#4
            pin4 = gpio.OpenPin(contact_pin4);
            pinValue4 = GpioPinValue.Low;
            pin4.Write(pinValue4);
            pin4.SetDriveMode(GpioPinDriveMode.Output);

            //#5
            pin5 = gpio.OpenPin(contact_pin5);
            pinValue5 = GpioPinValue.Low;
            pin5.Write(pinValue5);
            pin5.SetDriveMode(GpioPinDriveMode.Output);

            //#6
            pin6 = gpio.OpenPin(contact_pin6);
            pinValue6 = GpioPinValue.Low;
            pin6.Write(pinValue6);
            pin6.SetDriveMode(GpioPinDriveMode.Output);

            //#TADJ
            pinTADJ1 = gpio.OpenPin(TADJ_PIN1);
            pinTADJ2 = gpio.OpenPin(TADJ_PIN2);
            pinTADJ3 = gpio.OpenPin(TADJ_PIN3);
            pinTADJ4 = gpio.OpenPin(TADJ_PIN4);
            pinValueTADJ1 = GpioPinValue.Low;
            pinValueTADJ2 = GpioPinValue.Low;
            pinValueTADJ3 = GpioPinValue.Low;
            pinValueTADJ4 = GpioPinValue.Low;
            pinTADJ1.Write(pinValueTADJ1);
            pinTADJ2.Write(pinValueTADJ2);
            pinTADJ3.Write(pinValueTADJ3);
            pinTADJ4.Write(pinValueTADJ4);
            pinTADJ1.SetDriveMode(GpioPinDriveMode.Output);
            pinTADJ2.SetDriveMode(GpioPinDriveMode.Output);
            pinTADJ3.SetDriveMode(GpioPinDriveMode.Output);
            pinTADJ4.SetDriveMode(GpioPinDriveMode.Output);

            // GpioStatus.Text = "GPIO pin initialized correctly.";=======This makes it break, Why?===========
        }

              //LED_TO_PI_Listeners---------------------------------------------------------------------
              //LED_TO_PI_1
              private void led2pi_Pin1_ValueChanged(GpioPin sender, GpioPinValueChangedEventArgs e)
              {
                  // toggle the state of the LED every time the button is pressed
                  if (e.Edge == GpioPinEdge.FallingEdge)
                  {
                      led2piPinValue1 = (led2piPinValue1 == GpioPinValue.Low) ?
                          GpioPinValue.High : GpioPinValue.Low;
                      led2pi_Pin1.Write(led2piPinValue1);
                  }

                  // need to invoke UI updates on the UI thread because this event
                  // handler gets invoked on a separate thread.
                  var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                  {
                      if (e.Edge == GpioPinEdge.FallingEdge)
                      {
                          Led_to_Pi_Ellipse_1.Fill = (led2piPinValue1 == GpioPinValue.Low) ?
                              redBrush : grayBrush;
                      }
                  });
              }
              //LED_TO_PI_2
              private void led2pi_Pin2_ValueChanged(GpioPin sender, GpioPinValueChangedEventArgs e)
              {
                  // toggle the state of the LED every time the button is pressed
                  if (e.Edge == GpioPinEdge.FallingEdge)
                  {
                      led2piPinValue2 = (led2piPinValue2 == GpioPinValue.Low) ?
                          GpioPinValue.High : GpioPinValue.Low;
                      led2pi_Pin2.Write(led2piPinValue2);
                  }

                  // need to invoke UI updates on the UI thread because this event
                  // handler gets invoked on a separate thread.
                  var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                  {
                      if (e.Edge == GpioPinEdge.FallingEdge)
                      {
                          Led_to_Pi_Ellipse_2.Fill = (led2piPinValue2 == GpioPinValue.Low) ?
                              redBrush : grayBrush;
                      }
                  });
              }
              //LED_TO_PI_3
              private void led2pi_Pin3_ValueChanged(GpioPin sender, GpioPinValueChangedEventArgs e)
              {
                  // toggle the state of the LED every time the button is pressed
                  if (e.Edge == GpioPinEdge.FallingEdge)
                  {
                      led2piPinValue3 = (led2piPinValue3 == GpioPinValue.Low) ?
                          GpioPinValue.High : GpioPinValue.Low;
                      led2pi_Pin3.Write(led2piPinValue3);
                  }

                  // need to invoke UI updates on the UI thread because this event
                  // handler gets invoked on a separate thread.
                  var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                  {
                      if (e.Edge == GpioPinEdge.FallingEdge)
                      {
                          Led_to_Pi_Ellipse_3.Fill = (led2piPinValue3 == GpioPinValue.Low) ?
                              redBrush : grayBrush;
                      }
                  });
              }
              //LED_TO_PI_4
              private void led2pi_Pin4_ValueChanged(GpioPin sender, GpioPinValueChangedEventArgs e)
              {
                  // toggle the state of the LED every time the button is pressed
                  if (e.Edge == GpioPinEdge.FallingEdge)
                  {
                      led2piPinValue4 = (led2piPinValue4 == GpioPinValue.Low) ?
                          GpioPinValue.High : GpioPinValue.Low;
                      led2pi_Pin4.Write(led2piPinValue4);
                  }

                  // need to invoke UI updates on the UI thread because this event
                  // handler gets invoked on a separate thread.
                  var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                  {
                      if (e.Edge == GpioPinEdge.FallingEdge)
                      {
                          Led_to_Pi_Ellipse_4.Fill = (led2piPinValue4 == GpioPinValue.Low) ?
                              redBrush : grayBrush;
                      }
                  });
              }



              //LED_TO_PI_Listeners---------------------------------------------------------------------

        //#1
        private void ClickMe_Click(object sender, RoutedEventArgs e) 
        {
            {
                if (pinValue == GpioPinValue.High)
                {
                    pinValue = GpioPinValue.Low;
                    pin.Write(pinValue);
                    LED.Fill = grayBrush;
                }
                else
                {
                    pinValue = GpioPinValue.High;
                    pin.Write(pinValue);
                    LED.Fill = redBrush;
                }
            }
        }
        //#2
        private void ClickMe_Click2(object sender, RoutedEventArgs e) 
        {
            {
                if (pinValue2 == GpioPinValue.High)
                {
                    pinValue2 = GpioPinValue.Low;
                    pin2.Write(pinValue2);
                    LED2.Fill = grayBrush;
                }
                else
                {
                    pinValue2 = GpioPinValue.High;
                    pin2.Write(pinValue2);
                    LED2.Fill = redBrush;
                }
            }
        }
        //#3
        private void ClickMe_Click3(object sender, RoutedEventArgs e)
        {
            {
                if (pinValue3 == GpioPinValue.High)
                {
                    pinValue3 = GpioPinValue.Low;
                    pin3.Write(pinValue3);
                    LED3.Fill = grayBrush;
                }
                else
                {
                    pinValue3 = GpioPinValue.High;
                    pin3.Write(pinValue3);
                    LED3.Fill = redBrush;
                }
            }
        }
        //#4
        private void ClickMe_Click4(object sender, RoutedEventArgs e)
        {
            {
                if (pinValue4 == GpioPinValue.High)
                {
                    pinValue4 = GpioPinValue.Low;
                    pin4.Write(pinValue4);
                    LED4.Fill = grayBrush;
                }
                else
                {
                    pinValue4 = GpioPinValue.High;
                    pin4.Write(pinValue4);
                    LED4.Fill = redBrush;
                }
            }
        }
        //#5
        private void ClickMe_Click5(object sender, RoutedEventArgs e)
        {
            {
                if (pinValue5 == GpioPinValue.High)
                {
                    pinValue5 = GpioPinValue.Low;
                    pin5.Write(pinValue5);
                    LED5.Fill = grayBrush;
                }
                else
                {
                    pinValue5 = GpioPinValue.High;
                    pin5.Write(pinValue5);
                    LED5.Fill = redBrush;
                }
            }
        }
        //#6
        private void ClickMe_Click6(object sender, RoutedEventArgs e)
        {
            {
                if (pinValue6 == GpioPinValue.High)
                {
                    pinValue6 = GpioPinValue.Low;
                    pin6.Write(pinValue6);
                    LED6.Fill = grayBrush;
                }
                else
                {
                    pinValue6 = GpioPinValue.High;
                    pin6.Write(pinValue6);
                    LED6.Fill = redBrush;
                }
            }
        }
        //#AO (All On)
        private void ClickMe_ClickAO(object sender, RoutedEventArgs e)
        {
            {
                //AO-#1
                pinValue = GpioPinValue.High;
                pin.Write(pinValue);
                LED.Fill = redBrush;
                //AO-#2
                pinValue2 = GpioPinValue.High;
                pin2.Write(pinValue2);
                LED2.Fill = redBrush;
                //AO-#3
                pinValue3 = GpioPinValue.High;
                pin3.Write(pinValue3);
                LED3.Fill = redBrush;
                //AO-#4
                pinValue4 = GpioPinValue.High;
                pin4.Write(pinValue4);
                LED4.Fill = redBrush;
                //AO-#5
                pinValue5 = GpioPinValue.High;
                pin5.Write(pinValue5);
                LED5.Fill = redBrush;
                //AO-#6
                pinValue6 = GpioPinValue.High;
                pin6.Write(pinValue6);
                LED6.Fill = redBrush;
            }
        }
        //#Timer_ADJ_Pos_Message start
        private void TimerADJ_Pos_Message_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        //#TADJ
        private void ClickMe_ClickTADJ(object sender, RoutedEventArgs e)
        {
            {
                if (TADJ_POS == 0)
                {
                    pinValueTADJ4 = GpioPinValue.High;
                    pinValueTADJ3 = GpioPinValue.Low;
                    pinValueTADJ2 = GpioPinValue.Low;
                    pinValueTADJ1 = GpioPinValue.Low;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 1;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [1]";
                    return;
                }
                if (TADJ_POS == 1)
                {
                    pinValueTADJ4 = GpioPinValue.Low;
                    pinValueTADJ3 = GpioPinValue.High;
                    pinValueTADJ2 = GpioPinValue.Low;
                    pinValueTADJ1 = GpioPinValue.Low;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 2;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [2]";
                    return;
                }
                if (TADJ_POS == 2)
                {
                    pinValueTADJ4 = GpioPinValue.High;
                    pinValueTADJ3 = GpioPinValue.High;
                    pinValueTADJ2 = GpioPinValue.Low;
                    pinValueTADJ1 = GpioPinValue.Low;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 3;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [3]";
                    return;
                }
                if (TADJ_POS == 3)
                {
                    pinValueTADJ4 = GpioPinValue.Low;
                    pinValueTADJ3 = GpioPinValue.Low;
                    pinValueTADJ2 = GpioPinValue.High;
                    pinValueTADJ1 = GpioPinValue.Low;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 4;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [4]";
                    return;
                }
                if (TADJ_POS == 4)
                {
                    pinValueTADJ4 = GpioPinValue.High;
                    pinValueTADJ3 = GpioPinValue.Low;
                    pinValueTADJ2 = GpioPinValue.High;
                    pinValueTADJ1 = GpioPinValue.Low;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 5;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [5]";
                    return;
                }
                if (TADJ_POS == 5)
                {
                    pinValueTADJ4 = GpioPinValue.Low;
                    pinValueTADJ3 = GpioPinValue.High;
                    pinValueTADJ2 = GpioPinValue.High;
                    pinValueTADJ1 = GpioPinValue.Low;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 6;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [6]";
                    return;
                }
                if (TADJ_POS == 6)
                {
                    pinValueTADJ4 = GpioPinValue.High;
                    pinValueTADJ3 = GpioPinValue.High;
                    pinValueTADJ2 = GpioPinValue.High;
                    pinValueTADJ1 = GpioPinValue.Low;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 7;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [7]";
                    return;
                }
                if (TADJ_POS == 7)
                {
                    pinValueTADJ4 = GpioPinValue.Low;
                    pinValueTADJ3 = GpioPinValue.Low;
                    pinValueTADJ2 = GpioPinValue.Low;
                    pinValueTADJ1 = GpioPinValue.High;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 8;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [8]";
                    return;
                }
                if (TADJ_POS == 8)
                {
                    pinValueTADJ4 = GpioPinValue.High;
                    pinValueTADJ3 = GpioPinValue.Low;
                    pinValueTADJ2 = GpioPinValue.Low;
                    pinValueTADJ1 = GpioPinValue.High;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 9;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [9]";
                    return;
                }
                if (TADJ_POS == 9)
                {
                    pinValueTADJ4 = GpioPinValue.Low;
                    pinValueTADJ3 = GpioPinValue.High;
                    pinValueTADJ2 = GpioPinValue.Low;
                    pinValueTADJ1 = GpioPinValue.High;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 10;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [10]";
                    return;
                }
                if (TADJ_POS == 10)
                {
                    pinValueTADJ4 = GpioPinValue.High;
                    pinValueTADJ3 = GpioPinValue.High;
                    pinValueTADJ2 = GpioPinValue.Low;
                    pinValueTADJ1 = GpioPinValue.High;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 11;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [11]";
                    return;
                }
                if (TADJ_POS == 11)
                {
                    pinValueTADJ4 = GpioPinValue.Low;
                    pinValueTADJ3 = GpioPinValue.Low;
                    pinValueTADJ2 = GpioPinValue.High;
                    pinValueTADJ1 = GpioPinValue.High;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 12;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [12]";
                    return;
                }
                if (TADJ_POS == 12)
                {
                    pinValueTADJ4 = GpioPinValue.High;
                    pinValueTADJ3 = GpioPinValue.Low;
                    pinValueTADJ2 = GpioPinValue.High;
                    pinValueTADJ1 = GpioPinValue.High;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 13;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [13]";
                    return;
                }
                if (TADJ_POS == 13)
                {
                    pinValueTADJ4 = GpioPinValue.Low;
                    pinValueTADJ3 = GpioPinValue.High;
                    pinValueTADJ2 = GpioPinValue.High;
                    pinValueTADJ1 = GpioPinValue.High;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 14;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [14]";
                    return;
                }
                if (TADJ_POS == 14)
                {
                    pinValueTADJ4 = GpioPinValue.High;
                    pinValueTADJ3 = GpioPinValue.High;
                    pinValueTADJ2 = GpioPinValue.High;
                    pinValueTADJ1 = GpioPinValue.High;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 15;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [15]";
                    return;
                }
                if (TADJ_POS == 15)
                {
                    pinValueTADJ4 = GpioPinValue.Low;
                    pinValueTADJ3 = GpioPinValue.Low;
                    pinValueTADJ2 = GpioPinValue.Low;
                    pinValueTADJ1 = GpioPinValue.Low;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 0;
                    TimerADJ_Pos_Message.Text = "Timer ADJ. Position [0]";
                    return;
                }
                else
                {
                    pinValueTADJ4 = GpioPinValue.Low;
                    pinValueTADJ3 = GpioPinValue.Low;
                    pinValueTADJ2 = GpioPinValue.Low;
                    pinValueTADJ1 = GpioPinValue.Low;
                    pinTADJ4.Write(pinValueTADJ4);
                    pinTADJ3.Write(pinValueTADJ3);
                    pinTADJ2.Write(pinValueTADJ2);
                    pinTADJ1.Write(pinValueTADJ1);
                    TADJ_POS = 0;
                }
            }
        }
    }
}
// dont need- <TextBlock x:Name="GpioStatus" Text="Waiting to initialize GPIO..." Margin="11,50,11,11" TextAlignment="Center" FontSize="26.667" />

//for a text box that changes only after the first time it is triggered:
// for the box: hello world box-  <TextBox x:Name="HelloMessage" Text="Hello, World1!-Pin 40" Margin="10" IsReadOnly="True" TextChanged="HelloMessage_TextChanged"/>
// then:  // private void HelloMessage_TextChanged(object sender, TextChangedEventArgs e)
    //  {
    //  }
//then:  this.HelloMessage.Text = "Good Start! R=3V3 G=GND Pin 40";
